import acm.graphics.GCanvas;
import acm.program.Program;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class GUI extends Program
{
    private JTextField inputField;
    private JLabel tempResult;
    private JLabel fullResult;
    private JLabel weatherResult;

    public GUI()
    {
        start();
    }

    public void init()
    {
        JLabel inputLabel = new JLabel("Zip Code:");
        JLabel tempLabel = new JLabel("Temperature:");
        JLabel fullLabel = new JLabel("City, State:");
        JLabel weatherLabel = new JLabel("Weather:");

        GCanvas canvas = new GCanvas();
        this.add(canvas);

        canvas.add(inputLabel, 20, 20);
        canvas.add(tempLabel, 20, 50);
        canvas.add(fullLabel, 20, 80);
        canvas.add(weatherLabel, 20, 110);

        inputField = new JTextField();
        inputField.setSize(300, 20);
        canvas.add(inputField, 100, 20);

        JButton goButton = new JButton("Go");
        canvas.add(goButton, 400, 20);

        JButton clearButton = new JButton("Clear");
        canvas.add(clearButton, 450, 20);

        tempResult = new JLabel();
        canvas.add(tempResult, 100, 50);

        fullResult = new JLabel("   ");
        canvas.add(fullResult, 100, 80);

        weatherResult = new JLabel("   ");
        canvas.add(weatherResult, 100, 110);

        addActionListeners();
    }

    public static void main(String[] args)
    {
        GUI g = new GUI();
    }

    public void actionPerformed(ActionEvent ae)
    {
        String whichButton = ae.getActionCommand();

        switch (whichButton)
        {
            case "Go":
                Weather w = new Weather(inputField.getText());

                tempResult.setText(String.valueOf(w.getTemperature()));
                fullResult.setText(w.getFull());
                weatherResult.setText((w.getWeather()));

                tempResult.setSize(tempResult.getPreferredSize());
                fullResult.setSize(fullResult.getPreferredSize());
                weatherResult.setSize(weatherResult.getPreferredSize());
                break;

            case "Clear":
                inputField.setText("");
                tempResult.setText("");
                fullResult.setText("");
                weatherResult.setText("");
                break;

        }
    }
}